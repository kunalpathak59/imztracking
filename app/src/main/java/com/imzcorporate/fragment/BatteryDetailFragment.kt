package com.imzcorporate.fragment;

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.imzcorporate.R

class BatteryDetailFragment : BaseBottomSheetFragment() {

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"
        fun newInstance(sectionNumber: Int): BatteryDetailFragment {
            val fragment =BatteryDetailFragment()
            val args = Bundle()
            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }


    private lateinit var piechart_battery: PieChart

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.battery_view, container, false)

        return view
    }

    override fun initUi(view: View?) {
        piechart_battery = requireView().findViewById(R.id.piechart_battery);
    }

    override fun initialiseListener(view: View?) {
    }

    override fun setData(view: View?) {
        initPieChartBattery()
        setDataToPieChartBattery()

    }

    private fun initPieChartBattery() {
        piechart_battery.setUsePercentValues(true)
        piechart_battery.description.text = ""
        //hollow pie chart
        piechart_battery.isDrawHoleEnabled = false
        piechart_battery.setTouchEnabled(true)
        piechart_battery.setDrawEntryLabels(false)

        //adding padding
        //piechart_battery.setExtraOffsets(20f, 0f, 20f, 20f)
        piechart_battery.isRotationEnabled = true
        piechart_battery.setDrawEntryLabels(false)
        //pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        //pieChart.legend.isWordWrapEnabled = true
        piechart_battery.legend.isEnabled = false
    }

    private fun setDataToPieChartBattery() {
        piechart_battery.setUsePercentValues(true)
        val dataEntries = ArrayList<PieEntry>()
        dataEntries.add(PieEntry(12f, "Battery 0-10%"))
        dataEntries.add(PieEntry(36f, "Battery 11-50%"))
        dataEntries.add(PieEntry(45f, "Battery 51-75%"))
        dataEntries.add(PieEntry(07f, "Battery 76-100%"))
        dataEntries.add(PieEntry(19f, "Battery Charging"))

        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#FD0606"))
        colors.add(Color.parseColor("#f39c12"))
        colors.add(Color.parseColor("#0453B3"))
        colors.add(Color.parseColor("#00982a"))
        colors.add(Color.parseColor("#990096"))

        val dataSet = PieDataSet(dataEntries, "")
        val data = PieData(dataSet)

        val vf: ValueFormatter = object : ValueFormatter() {
            //value format here, here is the overridden method
            override fun getFormattedValue(value: Float): String {
                return "" + value.toInt()
            }
        }

        // In Percentage
        //data.setValueFormatter(PercentFormatter())
        // In Integer
        data.setValueFormatter(vf)
        dataSet.sliceSpace = 3f
        dataSet.colors = colors
        piechart_battery.data = data
        data.setValueTextSize(15f)
        data.setValueTextColor(Color.WHITE)
        piechart_battery.setExtraOffsets(5f, 10f, 5f, 5f)
        piechart_battery.animateY(2500, Easing.EaseInOutQuad)

        //create hole in center
        // piechart_battery.holeRadius = 58f
        // piechart_battery.transparentCircleRadius = 61f
        //piechart_battery.isDrawHoleEnabled = false
        //piechart_battery.setHoleColor(Color.WHITE)


        //add text in center
        piechart_battery.setDrawCenterText(false);
        //piechart_battery.centerText = "Lock\nUnlock"

        piechart_battery.invalidate()

    }
}