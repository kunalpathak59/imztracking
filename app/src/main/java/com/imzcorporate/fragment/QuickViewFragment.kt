package com.imzcorporate.fragment;

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.imzcorporate.R
import java.text.NumberFormat


class QuickViewFragment : BaseBottomSheetFragment() {
    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"
        fun newInstance(sectionNumber: Int): QuickViewFragment {
            val fragment =QuickViewFragment()
            val args = Bundle()
            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }



    lateinit var viewNew : View
    private lateinit var pieChartAvailable: PieChart
    private lateinit var pieChartUnavailable: PieChart
    private lateinit var piechart_assigned: PieChart

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.quick_view, container, false)
        viewNew = view

        return view
    }

    override fun initUi(view: View?) {
        pieChartAvailable = requireView().findViewById(R.id.piechart_available)
        pieChartUnavailable = requireView().findViewById(R.id.piechart_unavailable)
        piechart_assigned = requireView().findViewById(R.id.piechart_assigned)
    }

    override fun initialiseListener(view: View?) {
    }

    override fun setData(view: View?) {

        initPieChartAvailable()
        setDataToPieChartAvailable()

        initPieChartUnAvailable()
        setDataToPieChartUnAvailable()

        initPieChartAssigned()
        setDataToPieChartAssigned()

    }

    private fun initPieChartAssigned() {
        piechart_assigned.setUsePercentValues(true)
        piechart_assigned.description.text = ""
        //hollow pie chart
        piechart_assigned.isDrawHoleEnabled = false
        piechart_assigned.setTouchEnabled(true)
        piechart_assigned.setDrawEntryLabels(false)

        //adding padding
        piechart_assigned.setExtraOffsets(20f, 0f, 20f, 20f)
        piechart_assigned.setUsePercentValues(true)
        piechart_assigned.isRotationEnabled = true
        piechart_assigned.setDrawEntryLabels(false)
        //pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        //pieChart.legend.isWordWrapEnabled = true
        piechart_assigned.legend.isEnabled = false
    }

    private fun setDataToPieChartAssigned() {
        piechart_assigned.setUsePercentValues(true)
        val dataEntries = ArrayList<PieEntry>()
        dataEntries.add(PieEntry(52f, "Assigned"))
        dataEntries.add(PieEntry(26f, "Un Assigned"))

        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#2ecc72"))
        colors.add(Color.parseColor("#5584ff"))

        val dataSet = PieDataSet(dataEntries, "")
        val data = PieData(dataSet)

        val vf: ValueFormatter = object : ValueFormatter() {
            //value format here, here is the overridden method
            override fun getFormattedValue(value: Float): String {
                return "" + value.toInt()
            }
        }

        // In Percentage
        //data.setValueFormatter(PercentFormatter())
        // In Integer
        data.setValueFormatter(vf)
        dataSet.sliceSpace = 3f
        dataSet.colors = colors
        piechart_assigned.data = data
        data.setValueTextSize(15f)
        data.setValueTextColor(Color.WHITE)
        piechart_assigned.setExtraOffsets(5f, 10f, 5f, 5f)
        piechart_assigned.animateY(2500, Easing.EaseInOutQuad)

        //create hole in center
        piechart_assigned.holeRadius = 58f
        piechart_assigned.transparentCircleRadius = 61f
        piechart_assigned.isDrawHoleEnabled = true
        piechart_assigned.setHoleColor(Color.WHITE)

        //add text in center
        piechart_assigned.setDrawCenterText(true);
        piechart_assigned.centerText = "Assigned\nUn Assigned"

        piechart_assigned.invalidate()

    }

    private fun initPieChartAvailable() {
        pieChartAvailable.setUsePercentValues(true)
        pieChartAvailable.description.text = ""
        //hollow pie chart
        pieChartAvailable.isDrawHoleEnabled = false
        pieChartAvailable.setTouchEnabled(true)
        pieChartAvailable.setDrawEntryLabels(false)

        //adding padding
        pieChartAvailable.setExtraOffsets(20f, 0f, 20f, 20f)
        pieChartAvailable.setUsePercentValues(true)
        pieChartAvailable.isRotationEnabled = true
        pieChartAvailable.setDrawEntryLabels(false)
        //pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        //pieChart.legend.isWordWrapEnabled = true
        pieChartAvailable.legend.isEnabled = false

    }

    private fun setDataToPieChartAvailable() {
        pieChartAvailable.setUsePercentValues(true)
        val dataEntries = ArrayList<PieEntry>()
        dataEntries.add(PieEntry(52f, "In Transit"))
        dataEntries.add(PieEntry(26f, "Idling"))
        dataEntries.add(PieEntry(22f, "Halt"))

        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#00982a"))
        colors.add(Color.parseColor("#f39c12"))
        colors.add(Color.parseColor("#fd524d"))

        val dataSet = PieDataSet(dataEntries, "")
        val data = PieData(dataSet)

        val vf: ValueFormatter = object : ValueFormatter() {
            //value format here, here is the overridden method
            override fun getFormattedValue(value: Float): String {
                return "" + value.toInt()
            }
        }

        // In Percentage
        //data.setValueFormatter(PercentFormatter())
        // In Integer
        data.setValueFormatter(vf)
        dataSet.sliceSpace = 3f
        dataSet.colors = colors
        pieChartAvailable.data = data
        data.setValueTextSize(15f)
        data.setValueTextColor(Color.WHITE)
        pieChartAvailable.setExtraOffsets(5f, 10f, 5f, 5f)
        pieChartAvailable.animateY(2500, Easing.EaseInOutQuad)

        //create hole in center
        pieChartAvailable.holeRadius = 58f
        pieChartAvailable.transparentCircleRadius = 61f
        pieChartAvailable.isDrawHoleEnabled = true
        pieChartAvailable.setHoleColor(Color.WHITE)


        //add text in center
        pieChartAvailable.setDrawCenterText(true);
        pieChartAvailable.centerText = "Available"

        pieChartAvailable.invalidate()

    }

    private fun initPieChartUnAvailable() {
        pieChartUnavailable.setUsePercentValues(true)
        pieChartUnavailable.description.text = ""
        //hollow pie chart
        pieChartUnavailable.isDrawHoleEnabled = false
        pieChartUnavailable.setTouchEnabled(true)
        pieChartUnavailable.setDrawEntryLabels(false)

        //adding padding
        pieChartUnavailable.setExtraOffsets(20f, 0f, 20f, 20f)
        pieChartUnavailable.setUsePercentValues(true)
        pieChartUnavailable.isRotationEnabled = true
        pieChartUnavailable.setDrawEntryLabels(false)
        //pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        //pieChart.legend.isWordWrapEnabled = true
        pieChartUnavailable.legend.isEnabled = false

    }

    private fun setDataToPieChartUnAvailable() {
        pieChartUnavailable.setUsePercentValues(true)
        val dataEntries = ArrayList<PieEntry>()
        dataEntries.add(PieEntry(52f, "Disconnected"))
        dataEntries.add(PieEntry(26f, "Never Connected"))

        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#999c9e"))
        colors.add(Color.parseColor("#2a3e52"))

        val dataSet = PieDataSet(dataEntries, "")
        val data = PieData(dataSet)

        val vf: ValueFormatter = object : ValueFormatter() {
            //value format here, here is the overridden method
            override fun getFormattedValue(value: Float): String {
                return "" + value.toInt()
            }
        }

        // In Percentage
        //data.setValueFormatter(PercentFormatter())
        // In Integer
        data.setValueFormatter(vf)
        dataSet.sliceSpace = 3f
        dataSet.colors = colors
        pieChartUnavailable.data = data
        data.setValueTextSize(15f)
        data.setValueTextColor(Color.WHITE)
        pieChartUnavailable.setExtraOffsets(5f, 10f, 5f, 5f)
        pieChartUnavailable.animateY(2500, Easing.EaseInOutQuad)

        //create hole in center
        pieChartUnavailable.holeRadius = 58f
        pieChartUnavailable.transparentCircleRadius = 61f
        pieChartUnavailable.isDrawHoleEnabled = true
        pieChartUnavailable.setHoleColor(Color.WHITE)


        //add text in center
        pieChartUnavailable.setDrawCenterText(true);
        pieChartUnavailable.centerText = "UnAvailable"

        pieChartUnavailable.invalidate()


    }

}