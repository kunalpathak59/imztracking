package com.imzcorporate.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.imzcorporate.R
import com.imzcorporate.activities.DashBoardActivity
import com.imzcorporate.activities.DashBoardActivityNew
import com.imzcorporate.util.Utils

class NavigationFragment : Fragment(), View.OnClickListener {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.navigationdrawerview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.findViewById<LinearLayout>(R.id.ll_gridView).setOnClickListener(this)
        view.findViewById<LinearLayout>(R.id.ll_home).setOnClickListener(this)
        view.findViewById<LinearLayout>(R.id.llReoprt).setOnClickListener(this)
        view.findViewById<LinearLayout>(R.id.llReportHeader).setOnClickListener(this)
        view.findViewById<LinearLayout>(R.id.llDistance).setOnClickListener(this)
        view.findViewById<ImageView>(R.id.ivReportCollapseExpand)
        view.findViewById<ImageButton>(R.id.iv_logout).setOnClickListener(this)
        view.findViewById<LinearLayout>(R.id.ll_mapView).setOnClickListener(this)
        view.findViewById<LinearLayout>(R.id.ll_trackPlay).setOnClickListener(this)

        view.findViewById<TextView>(R.id.tv_app_version).text = "Version "+activity?.let {
            Utils.getAppVersion(it)
        }


       /* view.findViewById<TextView>(R.id.iv_logout).setOnClickListener(this)
        view.findViewById<TextView>(R.id.tv_home).setOnClickListener(this)
        view.findViewById<TextView>(R.id.tv_grid).setOnClickListener(this)

        view.findViewById<TextView>(R.id.tv_distance_report).setOnClickListener(this)
        view.findViewById<TextView>(R.id.tv_alerts_report).setOnClickListener(this)

        view.findViewById<LinearLayout>(R.id.llReoprt).setOnClickListener(this)
        view.findViewById<RelativeLayout>(R.id.rlReportHeader).setOnClickListener(this)
        view.findViewById<RelativeLayout>(R.id.ivReportCollapseExpand)
        view.findViewById<LinearLayout>(R.id.llProfile).setOnClickListener(this)

        view.findViewById<TextView>(R.id.tv_change_password).setOnClickListener(this)
        view.findViewById<ImageView>(R.id.ivArrow).setOnClickListener(this)

        // val name = activity?.let { PreferenceUtils.getString(it, PreferenceUtils.USER_NAME) }
        val name = "Demo"
        view.findViewById<TextView>(R.id.tv_name).setText("$name")
        view.findViewById<TextView>(R.id.tv_app_version).setText("version "+activity?.let {
            Utils.getAppVersion(
                it
            )
        })*/
    }

    override fun onClick(p0: View?) {
        (activity as DashBoardActivityNew).navigationClick(p0)
    }
}

