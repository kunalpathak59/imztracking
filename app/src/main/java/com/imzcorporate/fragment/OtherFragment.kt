package com.imzcorporate.fragment;

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.imzcorporate.R

class OtherFragment : BaseBottomSheetFragment() {

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"
        fun newInstance(sectionNumber: Int): OtherFragment {
            val fragment =OtherFragment()
            val args = Bundle()
            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }


    private lateinit var pieChartLockUnlock: PieChart
    private lateinit var pieChartGpsOut: PieChart

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.other_view, container, false)

        return view
    }

    override fun initUi(view: View?) {
        pieChartLockUnlock = requireView().findViewById(R.id.piechart_lock_unlock)
        pieChartGpsOut = requireView().findViewById(R.id.piechart_gps_out)

    }

    override fun initialiseListener(view: View?) {

    }

    override fun setData(view: View?) {
        initPieChartLockUnlock()
        setDataToPieChartLockUnlock()

        initPieChartGpsOut()
        setDataToPieChartGpsOut()
    }



    private fun setDataToPieChartLockUnlock() {
        pieChartLockUnlock.setUsePercentValues(true)
        val dataEntries = ArrayList<PieEntry>()
        dataEntries.add(PieEntry(52f, "Lock"))
        dataEntries.add(PieEntry(26f, "Unlock"))

        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#00982a"))
        colors.add(Color.parseColor("#f39c12"))

        val dataSet = PieDataSet(dataEntries, "")
        val data = PieData(dataSet)

        val vf: ValueFormatter = object : ValueFormatter() {
            //value format here, here is the overridden method
            override fun getFormattedValue(value: Float): String {
                return "" + value.toInt()
            }
        }

        // In Percentage
        //data.setValueFormatter(PercentFormatter())
        // In Integer
        data.setValueFormatter(vf)
        dataSet.sliceSpace = 3f
        dataSet.colors = colors
        pieChartLockUnlock.data = data
        data.setValueTextSize(15f)
        data.setValueTextColor(Color.WHITE)
        pieChartLockUnlock.setExtraOffsets(5f, 10f, 5f, 5f)
        pieChartLockUnlock.animateY(2500, Easing.EaseInOutQuad)

        //create hole in center
        pieChartLockUnlock.holeRadius = 58f
        pieChartLockUnlock.transparentCircleRadius = 61f
        pieChartLockUnlock.isDrawHoleEnabled = true
        pieChartLockUnlock.setHoleColor(Color.WHITE)


        //add text in center
        pieChartLockUnlock.setDrawCenterText(true);
        pieChartLockUnlock.centerText = "Lock\nUnlock"

        pieChartLockUnlock.invalidate()

    }

    private fun initPieChartLockUnlock() {
        pieChartLockUnlock.setUsePercentValues(true)
        pieChartLockUnlock.description.text = ""
        //hollow pie chart
        pieChartLockUnlock.isDrawHoleEnabled = false
        pieChartLockUnlock.setTouchEnabled(true)
        pieChartLockUnlock.setDrawEntryLabels(false)

        //adding padding
        pieChartLockUnlock.setExtraOffsets(20f, 0f, 20f, 20f)
        pieChartLockUnlock.setUsePercentValues(true)
        pieChartLockUnlock.isRotationEnabled = true
        pieChartLockUnlock.setDrawEntryLabels(false)
        //pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        //pieChart.legend.isWordWrapEnabled = true
        pieChartLockUnlock.legend.isEnabled = false
    }


    private fun setDataToPieChartGpsOut() {

        pieChartGpsOut.setUsePercentValues(true)
        val dataEntries = ArrayList<PieEntry>()
        dataEntries.add(PieEntry(52f, "GPS"))
        dataEntries.add(PieEntry(26f, "Out of Reach"))

        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#0453B3"))
        colors.add(Color.parseColor("#333333"))

        val dataSet = PieDataSet(dataEntries, "")
        val data = PieData(dataSet)

        val vf: ValueFormatter = object : ValueFormatter() {
            //value format here, here is the overridden method
            override fun getFormattedValue(value: Float): String {
                return "" + value.toInt()
            }
        }

        // In Percentage
        //data.setValueFormatter(PercentFormatter())
        // In Integer
        data.setValueFormatter(vf)
        dataSet.sliceSpace = 3f
        dataSet.colors = colors
        pieChartGpsOut.data = data
        data.setValueTextSize(15f)
        data.setValueTextColor(Color.WHITE)
        pieChartGpsOut.setExtraOffsets(5f, 10f, 5f, 5f)
        pieChartGpsOut.animateY(2500, Easing.EaseInOutQuad)

        //create hole in center
        pieChartGpsOut.holeRadius = 58f
        pieChartGpsOut.transparentCircleRadius = 61f
        pieChartGpsOut.isDrawHoleEnabled = true
        pieChartGpsOut.setHoleColor(Color.WHITE)



        //add text in center
        pieChartGpsOut.setDrawCenterText(true);
        pieChartGpsOut.centerText = "GPS\nOut of Reach"

        pieChartGpsOut.invalidate()

    }

    private fun initPieChartGpsOut() {
        pieChartGpsOut.setUsePercentValues(true)
        pieChartGpsOut.description.text = ""
        //hollow pie chart
        pieChartGpsOut.isDrawHoleEnabled = false
        pieChartGpsOut.setTouchEnabled(true)
        pieChartGpsOut.setDrawEntryLabels(false)

        //adding padding
        pieChartGpsOut.setExtraOffsets(20f, 0f, 20f, 20f)
        pieChartGpsOut.setUsePercentValues(true)
        pieChartGpsOut.isRotationEnabled = true
        pieChartGpsOut.setDrawEntryLabels(false)
        //pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        //pieChart.legend.isWordWrapEnabled = true
        pieChartGpsOut.legend.isEnabled = false
    }
}