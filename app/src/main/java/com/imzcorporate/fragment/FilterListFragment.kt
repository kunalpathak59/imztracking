package com.imzcorporate.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.imzcorporate.R
import com.imzcorporate.adapter.FilterListAdapter
import com.imzcorporate.model.FilterListModel
import com.imzcorporate.util.ExecutorUtils
import com.imzcorporate.util.OnTextChangedListener
import kotlinx.android.synthetic.main.fragment_devices.view.*
import java.util.*

class FilterListFragment(
    context: Context?,
    private val arrayFilterListModel: ArrayList<FilterListModel>,
    private val title: String?
) :
    BaseBottomSheetFragment() {
    private var onDeviceSelectionListener: OnDeviceSelectionListener? = null
    private var filterListAdapter: FilterListAdapter? = null
    private var timer: Timer? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_devices, container, false)
    }

    override fun initUi(view: View) {
        val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view)
        val txt_header: TextView = view.findViewById(R.id.txt_heading)

        txt_header.text = title
        recyclerView.layoutManager = LinearLayoutManager(context)
        filterListAdapter = FilterListAdapter(requireActivity(),object :FilterListAdapter.OnDeviceSelectionListener{
            override fun onDeviceSelected(filterListModel: FilterListModel) {
                onDeviceSelectionListener?.onDeviceSelected(filterListModel)
                dismissAllowingStateLoss()
            }

        },arrayFilterListModel)
        recyclerView.adapter = filterListAdapter
        filterListAdapter!!.setVehicleStatusModels(arrayFilterListModel)
    }

    override fun initialiseListener(view: View) {
//        val editText = view.findViewById<EditText>(R.id.et_search)
        view.et_search.addTextChangedListener(object : OnTextChangedListener() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (timer != null) {
                    timer!!.cancel()
                    timer = null
                }
                timer = Timer()
                timer!!.schedule(object : TimerTask() {
                    override fun run() {
                        ExecutorUtils.getInstance().executeOnMain {
                            val filterModel =
                                ArrayList<FilterListModel>()
                            for (vehicleStatusModel in arrayFilterListModel) {
                                if (vehicleStatusModel.name.lowercase(Locale.getDefault())
                                        .contains(view.et_search.text.toString().trim { it <= ' ' }
                                            .lowercase(Locale.getDefault()))
                                ) filterModel.add(vehicleStatusModel)
                            }
                            filterListAdapter!!.setVehicleStatusModels(filterModel)
                            println("filterModel: ${filterModel.size}")
                        }
                    }
                }, 1000)
            }
        })
    }

    override fun setData(view: View) {}

    fun setOnDeviceSelectionListener(onDeviceSelectionListener: OnDeviceSelectionListener) {
        this.onDeviceSelectionListener = onDeviceSelectionListener
    }

    interface OnDeviceSelectionListener {
        fun onDeviceSelected(filterListModel: FilterListModel)
    }

    companion object {
        val TAG = FilterListFragment::class.java.canonicalName
    }


}