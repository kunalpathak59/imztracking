package com.imzcorporate.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public abstract class BaseBottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initUi(view);
        initialiseListener(view);
        setData(view);

        setUpRecycler(view);
        setUpViewPager(view);
    }


    public abstract void initUi(View view);

    public abstract void initialiseListener(View view);

    public abstract void setData(View view);

    public void setUpRecycler(View view){}

    public void setUpViewPager(View view){}

    @Override
    public void onClick(View v) {

    }
}
