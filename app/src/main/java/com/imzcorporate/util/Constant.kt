package com.imzcorporate.util

import java.util.regex.Pattern

object Constant {


    val EMAIL_ADDRESS_PATTERN = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )

    enum class RequestStatus(val value: String) {
        ALL("All"),
        ASSIGNED("Assigned"),
        AVAILABLE("Available"),
        DISCONNECTED("Disconnected"),
        IDLE("Idle"),
        LOCK("Lock"),
        MOTION("Motion"),
        STOP("Stop"),
        UN_ASSIGNED("Un Assigned"),
        UNLOCK("Unlock")
    }
}
