package com.imzcorporate.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.text.TextUtils
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.text.SimpleDateFormat
import java.util.*

object Utils {

    @JvmStatic
    fun milesToKilometres(miles: Double): Double {
        return miles * 1.60934
    }

    @JvmStatic
    fun metresToKilometres(metres: Double): Double {
        return metres / 1000
    }

    @JvmStatic
    fun knotsToKilometrePerHour(knots : Double): Double{
        return knots * 1.852
    }

    fun dpToPx(dp: Int): Float {
        val scale = Resources.getSystem().displayMetrics.density
        return dp * scale + 0.5f
    }


    @JvmStatic
    fun getAddressFromLatLng(mContext: Context?, latitude: Double, longitude: Double): String? {
        var address = ""
        try {
            val addresses: List<Address>
            val geocoder = Geocoder(mContext, Locale.getDefault())
            if (Geocoder.isPresent()) {
                if (mContext != null && !TextUtils.isEmpty(latitude.toString()) && !TextUtils.isEmpty(
                        longitude.toString()
                    )
                ) {
                    try {
                        val location = Location("location")
                        location.longitude = longitude
                        location.latitude = latitude
                        addresses =
                            geocoder.getFromLocation(location.latitude, location.longitude, 1)
                        if (addresses.isNotEmpty()) address = addresses[0].getAddressLine(0)

                        /* if (addresses != null && addresses.size() > 0) {
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();
                            String local = addresses.get(0).getSubLocality();
                            //String knownName = addresses.get(0).getFeatureName();
                            address = local + ", " + city + " , " + state + ", " + country + ", " + postalCode;
                        }*/
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return address
    }


    fun hideSoftKeyboard(context: Context?) {
        if (context != null) {
            if ((context as AppCompatActivity).currentFocus != null) {
                val inputMethodManager =
                    context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(
                    context.currentFocus!!.windowToken, 0
                )
            }
        }
    }


     fun isNetworkConnected(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val networkCapabilities =
                connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
            return networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val activeNetworkInfo = connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION") return activeNetworkInfo.isAvailable
        }
    }

     fun checkForInternet(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            // if the android version is below M
            @Suppress("DEPRECATION") val networkInfo = connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION") return networkInfo.isConnected
        }
    }

    fun getDateFromMilliSec(dateStrMilli: String): String? {
        var dateTime = ""
        if (!dateStrMilli.isEmpty() && dateStrMilli != "null") {
            try {
                val dateMilli = dateStrMilli.toLong()
                val newDate = Date(dateMilli)
                val simpleDateFormat =
                    SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                dateTime = simpleDateFormat.format(newDate)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return dateTime
    }

    fun checkCameraStoragePermission(context: Context): Boolean {
        if (ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(
                    context as Activity,
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ), 232
                )
            }
            return false
        }
        return true
    }

    @JvmStatic
     fun getAppVersion(context: Context): String {
        val packageInfo: PackageInfo =
            context.packageManager.getPackageInfo(context.packageName, 0)
        return packageInfo.versionName
    }

}
