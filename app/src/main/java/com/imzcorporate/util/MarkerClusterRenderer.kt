package com.imzcorporate.util

import android.content.Context
import android.graphics.Bitmap
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator
import com.imzcorporate.R
import com.imzcorporate.model.Vehicle_ItemDetail

class MarkerClusterRenderer(context: Context, map: GoogleMap, clusterManager: ClusterManager<Vehicle_ItemDetail>?):
    DefaultClusterRenderer<Vehicle_ItemDetail>(context, map, clusterManager) {
    private val MARKER_DIMENSION = 98  // 2
    private var iconGenerator: IconGenerator? = null
    private var markerImageView: ImageView? = null
    private var mContext: Context? = null
    init {
        mContext = context
        iconGenerator = IconGenerator(context)  // 3
        markerImageView = ImageView(context)
        markerImageView!!.layoutParams = ViewGroup.LayoutParams(MARKER_DIMENSION, MARKER_DIMENSION)
       // markerImageView!!.setBackgroundColor(getColor(R.color.Python))
        iconGenerator!!.setContentView(markerImageView)

    }

    override fun onBeforeClusterItemRendered(
        item: Vehicle_ItemDetail,
        markerOptions: MarkerOptions
    ) {
        super.onBeforeClusterItemRendered(item, markerOptions)

      // markerImageView?.setImageResource(R.drawable.blue_marker);  // 6
        var icon = iconGenerator?.makeIcon();  // 7
        markerOptions?.icon(icon?.let { BitmapDescriptorFactory.fromBitmap(it) });  // 8
        markerOptions.title(item?.title);


    }

    override fun onBeforeClusterRendered(
        cluster: Cluster<Vehicle_ItemDetail>,
        markerOptions: MarkerOptions
    ) {
        super.onBeforeClusterRendered(cluster, markerOptions)
       /* iconGenerator?.setTextAppearance(R.style.AppTheme_WhiteTextAppearance);

        iconGenerator?.setBackground(
            ContextCompat.getDrawable(mContext!!, R.drawable.blue_marker));

          var icon = iconGenerator?.makeIcon(cluster.size.toString());
        markerOptions.icon(icon?.let { BitmapDescriptorFactory.fromBitmap(it) });
*/
    }
}