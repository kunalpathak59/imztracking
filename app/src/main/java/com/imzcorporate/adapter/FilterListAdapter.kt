package com.imzcorporate.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.imzcorporate.R
import com.imzcorporate.model.FilterListModel
import kotlinx.android.synthetic.main.row_devices.view.*
import java.util.ArrayList

    class FilterListAdapter(context: Context,
                            private val onDeviceSelectionListener:OnDeviceSelectionListener,
                            private val filterList: ArrayList<FilterListModel>) : RecyclerView.Adapter<FilterListAdapter.FilterListHolder>() {
        var context = context
    private var arrayFilterListModel = ArrayList<FilterListModel>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterListHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_devices, parent, false)
            return FilterListHolder(view)
        }

        override fun onBindViewHolder(holder: FilterListHolder, position: Int) {
            val data = arrayFilterListModel[position]

           holder.itemView.tv_device.setOnClickListener{
               onDeviceSelectionListener.onDeviceSelected(data)
                }
            holder.bind(context , data , position)
        }

        override fun getItemCount(): Int {
            return arrayFilterListModel.size
        }

        class FilterListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(context: Context, filterListModel: FilterListModel, position: Int) {
                itemView.tv_device.text = filterListModel.name

            }
        }
    fun setVehicleStatusModels(arrayFilterListModelNew: ArrayList<FilterListModel>?) {
        arrayFilterListModel.clear()
        this.arrayFilterListModel.addAll(arrayFilterListModelNew!!)
        notifyDataSetChanged()
    }
    interface OnDeviceSelectionListener {
        fun onDeviceSelected(filterListModel: FilterListModel)
    }

}