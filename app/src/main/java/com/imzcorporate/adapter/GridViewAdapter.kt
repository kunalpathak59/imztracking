package com.imzcorporate.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.imzcorporate.R
import com.imzcorporate.model.GridListModel

var movieList: List<GridListModel>? = null

class GridViewAdapter(
    context: Context,
    private var deviceList: List<GridListModel>,
    private var itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<GridViewAdapter.GridAdapterViewHolder>()  {
    var activity = context
    var movieList = deviceList


    fun GridViewAdapter(movieList: List<GridListModel?>?) {
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_grid_list, parent, false)
        return GridAdapterViewHolder(view, itemClickListener)
    }

    override fun onBindViewHolder(holder: GridAdapterViewHolder, position: Int) {
        holder.bind(activity, deviceList[position], position)
        val isExpanded: Boolean = deviceList.get(position).isExpanded()
        holder.llDetails.setVisibility(if (isExpanded) View.VISIBLE else View.GONE)

        holder.ivCollapseExpand.setOnClickListener {

            var movie_: GridListModel? = movieList?.get(position)
            if (movie_ != null) {
                movie_.setExpanded(!movie_.expanded)
            }
            notifyItemChanged(position);

            if (holder.llDetails.visibility == View.VISIBLE) {
                holder.llDetails.visibility = View.GONE
                holder.ivCollapseExpand.setImageResource(R.drawable.icon_ionic_ios_arrow_left)
            } else {
                holder.llDetails.visibility = View.VISIBLE
                holder.ivCollapseExpand.setImageResource(R.drawable.icon_ionic_ios_arrow_down)
              /*  var movie_: GridListModel? = movieList?.get(position)


                if (movie_ != null) {
                    movie_.expanded(!movie_.expanded)
                }
                notifyItemChanged(position);*/


            }
        }

    }

    override fun getItemCount(): Int {
        return deviceList.size
    }

    class GridAdapterViewHolder(itemView: View, private val itemClickListener: OnItemClickListener) : RecyclerView.ViewHolder(itemView) {
        private val rlHeader: RelativeLayout = itemView.findViewById(R.id.rlHeader)
        private val ivHeader: ImageView = itemView.findViewById(R.id.ivHeader)
        private val tvIotName: TextView = itemView.findViewById(R.id.tvIotName)
         val ivCollapseExpand: ImageView = itemView.findViewById(R.id.ivCollapseExpand)
         val llDetails: LinearLayout = itemView.findViewById(R.id.llDetails)
        private val tv_date: TextView = itemView.findViewById(R.id.tv_date)
        private val iv_status: ImageView = itemView.findViewById(R.id.iv_status)
        private val ivLockStatus: ImageView = itemView.findViewById(R.id.ivLockStatus)
        private val txt_deviceType:TextView = itemView.findViewById(R.id.txt_deviceType)
        private val txt_driverDetail: TextView = itemView.findViewById(R.id.txt_driverDetail)
        private val txt_speed: TextView = itemView.findViewById(R.id.txt_speed)
        private val txt_batteryPercentage: TextView = itemView.findViewById(R.id.txt_batteryPercentage)
        private val txt_address: TextView = itemView.findViewById(R.id.txt_address)

        fun bind(activity: Context, deviceModel: GridListModel, position: Int) {

            ivHeader.visibility = View.VISIBLE
            txt_address.text = deviceModel.address
            tvIotName.text = deviceModel.iotName
            tv_date.text = deviceModel.dateTime
            txt_deviceType.text = deviceModel.deviceType
            txt_driverDetail.text = deviceModel.driverDetail
            txt_speed.text = deviceModel.speed
            txt_batteryPercentage.text = deviceModel.batteryPercentage



            /* when (deviceModel.status) {
                 Constant.RequestStatus.ASSIGN_TO_TECHNICIAN.value -> {
                     tvTitleHeader.text = "Ready For Installation"
                     ivHeader.setImageResource(R.drawable.group_23898)
                     rlHeader.setBackgroundColor(Color.parseColor("#f2b538"))
                 }
                 Constant.RequestStatus.WAITING_FOR_APPROVAL.value -> {
                     tvTitleHeader.text = "Verification Pending"
                     ivHeader.visibility = View.GONE
                     rlHeader.setBackgroundColor(Color.parseColor("#f67f1a"))
                 }
                 Constant.RequestStatus.INSTALLED.value -> {
                     tvTitleHeader.text = "Device Installed"
                     ivHeader.setImageResource(R.drawable.certificate)
                     rlHeader.setBackgroundColor(Color.parseColor("#63c88c"))

                 }
             }*/



        }

    }

    interface OnItemClickListener {
        fun onClickMapView(position: Int, deviceModel: GridListModel)
    }
    fun setVehicleStatusModels(ints : Int){
        notifyItemChanged(ints);    }


}