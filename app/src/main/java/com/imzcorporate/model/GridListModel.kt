package com.imzcorporate.model

data class GridListModel(
    var id: Int,
    var iotName: String,
    var status: String,
    var lockStatus: String,
    var deviceType: String,
    var batteryPercentage: String,
    var speed: String,
    var driverDetail: String,
    var dateTime: String,
    var address: String,
    var queryStatus: String,
    var expanded : Boolean = true

) {

    fun isExpanded(): Boolean {
        return expanded
    }

    @JvmName("setExpanded1")
    fun setExpanded(expanded: Boolean) {
        this.expanded = expanded
    }
}
