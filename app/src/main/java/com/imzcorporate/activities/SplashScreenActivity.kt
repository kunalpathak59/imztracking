package com.imzcorporate.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Window
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.imzcorporate.R
import kotlinx.android.synthetic.main.activity_splash_screen.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash_screen)
        val slideAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        img_logo.startAnimation(slideAnimation)

        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }, 3000)

       /* val backgroundExecutor: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()

        // Execute a task in the background thread.
        backgroundExecutor.execute {
            // Your code logic goes here.
        }

        // Execute a task in the background thread after 3 seconds.
        backgroundExecutor.schedule({
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }, 3, TimeUnit.SECONDS)
        // Note: Remember to shut down the executor after using.
       // backgroundExecutor.shutdown(); // or backgroundExecutor.shutdownNow();
        */
    }
}