package com.imzcorporate.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.imzcorporate.R
import kotlinx.android.synthetic.main.toolbar_gradient.*

class TrackPlayActivity : AppCompatActivity(), OnMapReadyCallback,
    View.OnClickListener {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_play)
        initView()
        initListner()

    }

    private fun initView() {
        tv_toolbar_title_.text = "Track Play"
        iv_toolbar_search_card.visibility = View.GONE

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.track_play_maps) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun initListner() {

        iv_menu_back.setOnClickListener(this)
    }


    override fun onMapReady(gMap: GoogleMap) {
        mMap = gMap
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_menu_back -> {
                finish()
            }
        }
    }
}