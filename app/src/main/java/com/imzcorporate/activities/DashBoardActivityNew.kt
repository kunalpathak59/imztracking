package com.imzcorporate.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.imzcorporate.R
import com.imzcorporate.fragment.BatteryDetailFragment
import com.imzcorporate.fragment.FilterListFragment
import com.imzcorporate.fragment.OtherFragment
import com.imzcorporate.fragment.QuickViewFragment
import com.imzcorporate.model.FilterListModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.navigationdrawerview.*
import kotlinx.android.synthetic.main.toolbar_dashboard.*

class DashBoardActivityNew : AppCompatActivity(), View.OnClickListener {

    private var profileClicked = false
    private var reportClicked = false
    private val quickviewFragmentTag = "quik_view_fragment"
    private val batteryFragmentTag = "battery_fragment"
    private val otherFragmentTag = "other_fragment"
    private var quickViewFragment = QuickViewFragment()
    private var batteryDetailFragment = BatteryDetailFragment()
    private var otherFragment = OtherFragment()
    private var accountArrayFilterListModel = ArrayList<FilterListModel>()
    private var sub_accountArrayFilterListModel = ArrayList<FilterListModel>()
    private var selectedAccountId = 0
    private var selectedSubAccountId = 0
    private var btnArray: Array<Button>? = null
    private var fragmentCallerType = 1

    private var view_pager: ViewPager? = null
    private var tab_layout: TabLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initListener()
        setData()
        //setFragmentForInstallation(1)
        changeButtonColor(btnQuickView)
        initComponent()

    }
    private fun initComponent() {
        view_pager = findViewById<View>(R.id.view_pager) as ViewPager
        setupViewPager(view_pager!!)
        tab_layout = findViewById<View>(R.id.tab_layout) as TabLayout
        tab_layout!!.setupWithViewPager(view_pager)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter =
           SectionsPagerAdapter(
                supportFragmentManager
            )
        adapter.addFragment(
           QuickViewFragment.newInstance(
                1
            ), "Quick View"
        )
        adapter.addFragment(
            BatteryDetailFragment.newInstance(
                2
            ), "Battery View"
        )
        adapter.addFragment(
            OtherFragment.newInstance(
                3
            ), "Other"
        )
        viewPager.adapter = adapter
    }

   /* class PlaceholderFragment : Fragment() {
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView: View = inflater.inflate(R.layout.fragment_tabs_basic, container, false)
            val textView = rootView.findViewById<View>(R.id.section_label) as TextView
            textView.text =
                getString(R.string.section_format, requireArguments().getInt(ARG_SECTION_NUMBER))
            return rootView
        }

        companion object {
            private const val ARG_SECTION_NUMBER = "section_number"
            fun newInstance(sectionNumber: Int): PlaceholderFragment {
                val fragment = PlaceholderFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }
    }*/


    fun initListener() {
         tv_toolbar_title?.text = "Dashboard"

        // tv_change_password.visibility = View.GONE
         llReoprt.visibility = View.GONE

         iv_menu.setOnClickListener(this)
         atvMainAccount.setOnClickListener(this)
         atvsubAccount.setOnClickListener(this)
         ivMainAccountClear.setOnClickListener(this)
         ivSubAccountClear.setOnClickListener(this)
         btnQuickView.setOnClickListener(this)
         btnBatteryView.setOnClickListener(this)
         btnOther.setOnClickListener(this)
         iv_menu.setImageResource(R.drawable.menu_btn)

    }

    fun setData(){
        setUpAtvAdapter()
        btnArray = arrayOf(btnQuickView, btnBatteryView, btnOther)
    }
    private class SectionsPagerAdapter(manager: FragmentManager?) :
        FragmentPagerAdapter(manager!!) {
        private val mFragmentList: MutableList<Fragment> = java.util.ArrayList()
        private val mFragmentTitleList: MutableList<String> = java.util.ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }


    fun navigationClick(view: View?) {
        when (view) {
           /* ivArrow -> {
                if (!profileClicked) {
                    profileClicked = true
                    ivArrow.setImageResource(R.drawable.icon_arrow_down)
                    tv_change_password.visibility = View.VISIBLE
                } else {
                    profileClicked = false
                    ivArrow.setImageResource(R.drawable.icon_arrow_forward)
                    tv_change_password.visibility = View.GONE
                }
            }*/

            iv_logout -> {
                drawer_layout.closeDrawer(Gravity.LEFT)
                // PreferenceUtils.clearPref(this)
                startActivity(Intent(this, LoginActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                })

                finish()
            }

            ll_home -> {
                drawer_layout.closeDrawer(Gravity.LEFT)
            }

            ll_gridView -> {
                drawer_layout.closeDrawer(Gravity.LEFT)
                Handler(Looper.getMainLooper()).postDelayed(
                    { startActivity(Intent(this, GridViewActivity::class.java))
                        },
                    100
                )

            }

            llReportHeader -> {
                if (!reportClicked) {
                    reportClicked = true
                    llReoprt.visibility = View.VISIBLE
                    ivReportCollapseExpand.setImageResource(R.drawable.icon_arrow_down)
                } else {
                    reportClicked = false
                    llReoprt.visibility = View.GONE
                    ivReportCollapseExpand.setImageResource(R.drawable.icon_arrow_forward)
                }
            }

            llDistance -> {

                reportClicked = false
                llReoprt.visibility = View.GONE
                ivReportCollapseExpand.setImageResource(R.drawable.icon_arrow_forward)
                drawer_layout.closeDrawer(Gravity.LEFT)

            }
            ll_mapView ->{
                drawer_layout.closeDrawer(Gravity.LEFT)
                Handler(Looper.getMainLooper()).postDelayed({
                    startActivity(Intent(this, MapsViewActivity::class.java))
                    },
                    100)
            }
            ll_trackPlay ->{

                drawer_layout.closeDrawer(Gravity.LEFT)
                Handler(Looper.getMainLooper()).postDelayed({
                    startActivity(Intent(this, ReportActivity::class.java))
                },
                    100)
            }
            /*
            tv_alerts_report -> {
               // startActivity(Intent(this, AlertsNotificationActivity::class.java))
                drawer_layout.closeDrawer(Gravity.LEFT)
            }*/

        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_menu -> {
                drawer_layout.openDrawer(GravityCompat.START)
            }
            R.id.atvMainAccount -> {
                callFilterListFragment(accountArrayFilterListModel, atvMainAccount, 1,"Select Account")
            }
            R.id.atvsubAccount -> {
                callFilterListFragment(sub_accountArrayFilterListModel, atvsubAccount, 2,"Select Sub Account")
            }
            R.id.ivMainAccountClear -> {
                filterAtvClear(atvMainAccount, ivMainAccountClear)
            }
            R.id.ivSubAccountClear -> {
                filterAtvClear(atvsubAccount, ivSubAccountClear)
            }
          /*  R.id.btnQuickView -> {
                changeButtonColor(btnQuickView)
                setFragmentForInstallation(1)
            }
            R.id.btnBatteryView -> {
                changeButtonColor(btnBatteryView)
                setFragmentForInstallation(2)
            }
            R.id.btnOther -> {
                changeButtonColor(btnOther)
                setFragmentForInstallation(3)
            }*/

        }
    }

    private fun filterAtvClear(atv: AutoCompleteTextView, ivCross: ImageView) {
        atv.setText("")
        ivCross.visibility = View.GONE
    }

    private fun setUpAtvAdapter() {
        try {
            val arraySubAccount = ArrayList<FilterListModel>()
            val arrayAccount = ArrayList<FilterListModel>()

            for (j in 0 until 10) {
                val subAccount = FilterListModel()
                subAccount.name = "Sub Account $j"
                subAccount.id = j+1
                arraySubAccount.add(subAccount)
            }
            for (k in 0 until 10) {
                val accountName = FilterListModel()
                accountName.name = "Account $k"
                accountName.id = k+1
                arrayAccount.add(accountName)
            }

            sub_accountArrayFilterListModel.addAll(arraySubAccount)
            accountArrayFilterListModel.addAll(arrayAccount)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun callFilterListFragment(
        arrayFilterListModel: ArrayList<FilterListModel>,
        atv: AutoCompleteTextView,
        caller: Int,
        title: String) {
        val filterListFragment = FilterListFragment(this, arrayFilterListModel, title)
        filterListFragment.setOnDeviceSelectionListener(object :
            FilterListFragment.OnDeviceSelectionListener {
            override fun onDeviceSelected(filterListModel: FilterListModel) {
                atv.setText(filterListModel.name)
                when (caller) {
                    1 -> {
                        ivMainAccountClear.visibility = View.VISIBLE
                        selectedAccountId = filterListModel.id
                        Log.i("AccountId:", selectedAccountId.toString())
                        //vehicleListOnClickListener()
                    }
                    2 -> {
                        ivSubAccountClear.visibility = View.VISIBLE
                        selectedSubAccountId = filterListModel.id
                        Log.i("Sub AccountId:", selectedSubAccountId.toString())
                        //vehicleListOnClickListener()
                    }
                }
            }
        })

        filterListFragment.show(supportFragmentManager, FilterListFragment.TAG)
    }

    private fun changeButtonColor(clickedButton: Button) {
        btnArray!!.forEach {
            if (it == clickedButton) {
                it.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.oliveGreen)
                it.setTextColor(Color.parseColor("#ffffff"))
            } else {
                it.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.white)
                it.setTextColor(Color.parseColor("#219797"))

            }
        }
    }

    private fun setFragmentForInstallation(screenPosition: Int) {

        when (screenPosition) {
            1 -> {
                fragmentCallerType = 1
                supportFragmentManager.beginTransaction()
                    .replace(R.id.frameLayoutDashboard, quickViewFragment)
                    .addToBackStack(quickviewFragmentTag).commit()
            }
            2 -> {
                fragmentCallerType = 2
                supportFragmentManager.beginTransaction()
                    .replace(R.id.frameLayoutDashboard, batteryDetailFragment)
                    .addToBackStack(batteryFragmentTag).commit()
            }
            3 -> {
                fragmentCallerType = 3
                supportFragmentManager.beginTransaction()
                    .replace(R.id.frameLayoutDashboard, otherFragment)
                    .addToBackStack(otherFragmentTag).commit()
            }
        }

    }

    override fun onBackPressed() {
        val fm: FragmentManager = supportFragmentManager
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStack()
        }
        finish()
        super.onBackPressed()
    }
}