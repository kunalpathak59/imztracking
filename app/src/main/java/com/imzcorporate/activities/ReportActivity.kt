package com.imzcorporate.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.SupportMapFragment
import com.imzcorporate.R
import kotlinx.android.synthetic.main.toolbar_gradient.*

class ReportActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)

        initView()
        initListner()
    }


    private fun initView() {
        tv_toolbar_title_.text = "Track Play"
        iv_toolbar_search_card.visibility = View.GONE
        iv_toolbar_filter_card.visibility = View.VISIBLE

    }

    private fun initListner() {

        iv_menu_back.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_menu_back -> {
                finish()
            }
        }
    }

}