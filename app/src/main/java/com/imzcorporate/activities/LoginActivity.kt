package com.imzcorporate.activities

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.appcompat.app.AppCompatActivity
import com.imzcorporate.R
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.btn_login
import kotlinx.android.synthetic.main.activity_login_two.*
import kotlinx.android.synthetic.main.activity_login_two.*
import kotlinx.android.synthetic.main.activity_login.btn_login
import kotlinx.android.synthetic.main.activity_login_two.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_two)

       // initListner()
        btn_login.setOnClickListener {
            startActivity(Intent(this, DashBoardActivityNew::class.java))
            finish()
        }
        imgImz.setOnClickListener {
            startActivity(Intent(this, DashBoardActivity::class.java))
            finish()
        }

    }

  /*  private fun initListner() {
        ib_showPassword.setOnClickListener {
            if (et_password.transformationMethod
                    .equals(PasswordTransformationMethod.getInstance())
            ) {
                ib_showPassword.setImageResource(R.drawable.ic_hide_password)
                //Show Password
                et_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                ib_showPassword.setImageResource(R.drawable.ic_show_password)
                //Hide Password
                et_password.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            et_password.setSelection(et_password.text.length)
        }

        btn_login.setOnClickListener {
            startActivity(Intent(this, DashBoardActivity::class.java))
            finish()
        }
    }*/
}