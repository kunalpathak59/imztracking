package com.imzcorporate.activities

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.imzcorporate.R
import com.imzcorporate.adapter.GridViewAdapter
import com.imzcorporate.model.GridListModel
import com.imzcorporate.util.Constant
import com.imzcorporate.util.Utils
import kotlinx.android.synthetic.main.activity_grid_view.*
import kotlinx.android.synthetic.main.toolbar_gradient.*
import kotlinx.android.synthetic.main.toolbar_search_gradient.*
import java.util.*
import kotlin.collections.ArrayList

class GridViewActivity : AppCompatActivity(), View.OnClickListener {

    private var gridViewAdapter: GridViewAdapter? = null

    private var btnArray: Array<TextView>? = null
    private var deviceList = ArrayList<GridListModel>()
    private val mainDeviceList = ArrayList<GridListModel>()
    var searchText: String = ""
    private var status: String? = null
    private var listStatus: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_view)

        mainDeviceList.clear()
        for (i in 1..20){
            if (i %2 == 1) {
                val gridModel = GridListModel(
                    i,
                    "999999999$i / (DL1RT111$i)",
                    "connected",
                    "lock",
                    "G400",
                    "79%",
                    "65 km/hr",
                    "Driver $i/ 9999999999",
                    "28-04-2022 15:57:34",
                    "183, Udyog Vihar, Phase-IV, Sector 18, Gurugram- 122015, Haryana(India)",
                    "Available"
                )
                mainDeviceList.add(gridModel)
            }else{
                val gridModel = GridListModel(
                    i,
                    "999999999$i / (DL1RT111$i)",
                    "connected",
                    "lock",
                    "G400",
                    "79%",
                    "65 km/hr",
                    "Driver $i/ 9999999999",
                    "28-04-2022 15:57:34",
                    "183, Udyog Vihar, Phase-IV, Sector 18, Gurugram- 122015, Haryana(India)",
                    "Assigned"
                )
                mainDeviceList.add(gridModel)
            }
        }

        initView()
        initListner()

        changeButtonColor(tv_all_filter)
    }

    private fun initView() {
        tv_toolbar_title_.text = "Grid View - IoT Padlock"

        grid_list_recycler_view?.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        grid_list_recycler_view!!.visibility = View.VISIBLE

    }

    private fun initListner() {

        btnArray = arrayOf(tv_all_filter, tv_assigned_filter, tv_available_filter, tv_disconnect_filter, tv_idle_filter,
            tv_lock_filter, tv_motion_filter, tv_stop_filter, tv_unassigned_filter, tv_unlock_filter)

        iv_menu_back.setOnClickListener(this)
        tv_all_filter.setOnClickListener(this)
        tv_assigned_filter.setOnClickListener(this)
        tv_available_filter.setOnClickListener(this)
        tv_disconnect_filter.setOnClickListener(this)
        tv_idle_filter.setOnClickListener(this)
        tv_lock_filter.setOnClickListener(this)
        tv_motion_filter.setOnClickListener(this)
        tv_stop_filter.setOnClickListener(this)
        tv_unassigned_filter.setOnClickListener(this)
        tv_unlock_filter.setOnClickListener(this)
        iv_toolbar_search_icon.setOnClickListener(this)
        iv_searchbar_back.setOnClickListener(this)

        gridViewAdapter =
            GridViewAdapter(
                this,
                mainDeviceList,
                itemClickListener()
            )

        grid_list_recycler_view?.adapter = gridViewAdapter


        gridViewAdapter!!.notifyDataSetChanged()

        sv_toolbar_search?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                searchText = p0.toString()
                searchDeviceRequest(searchText)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, start: Int, before: Int, count: Int) {


            }

        })
    }

    @SuppressLint("SetTextI18n")
    fun searchDeviceRequest(searchStr: String) {
        if (searchStr.isNotEmpty()) {
            deviceList.clear()
            val search = searchStr.lowercase(Locale.getDefault())
            mainDeviceList.forEach {
                println(mainDeviceList)
                if (it.iotName != null) {
                    if (it.driverDetail.lowercase(Locale.getDefault())
                            .contains(search) || it.iotName.lowercase(Locale.getDefault())
                            .contains(search)
                    )
                        deviceList.add(it)
                    gridViewAdapter =
                        GridViewAdapter(
                            this,
                            deviceList,
                            itemClickListener()
                        )

                    grid_list_recycler_view?.adapter = gridViewAdapter

                    gridViewAdapter!!.notifyDataSetChanged()
                    grid_list_recycler_view!!.visibility = View.VISIBLE
                } else if (it.iotName.isNotEmpty()) {
                    if (it.iotName.lowercase(Locale.getDefault()).contains(search))
                        deviceList.add(it)

                    gridViewAdapter =
                        GridViewAdapter(
                            this,
                            deviceList,
                            itemClickListener()
                        )

                    gridViewAdapter!!.notifyDataSetChanged()
                    grid_list_recycler_view!!.visibility = View.VISIBLE
                }else{
                    gridViewAdapter =
                        GridViewAdapter(
                            this,
                            mainDeviceList,
                            itemClickListener()
                        )

                    gridViewAdapter!!.notifyDataSetChanged()
                    grid_list_recycler_view!!.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun changeButtonColor(clickedTextView: TextView) {
        btnArray!!.forEach {
            if (it == clickedTextView) {
                it.backgroundTintList = ContextCompat.getColorStateList(this, R.color.oliveGreen)
                it.setTextColor(Color.parseColor("#FFFFFF"))
            } else {
                it.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.light_grey)
                it.setTextColor(Color.parseColor("#2a3e52"))

            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_menu_back -> {
                finish()
            }
            R.id.tv_all_filter -> {
                changeButtonColor(tv_all_filter)
                filterRecords("All")
            }
            R.id.tv_assigned_filter -> {
                changeButtonColor(tv_assigned_filter)
                filterRecords("Assigned")
            }
            R.id.tv_available_filter -> {
                changeButtonColor(tv_available_filter)
                filterRecords("Available")
            }
            R.id.tv_disconnect_filter -> {
                changeButtonColor(tv_disconnect_filter)
                filterRecords("Disconnected")
            }
            R.id.tv_idle_filter -> {
                changeButtonColor(tv_idle_filter)
                filterRecords("Idle")
            }
            R.id.tv_lock_filter -> {
                changeButtonColor(tv_lock_filter)
                filterRecords("Lock")
            }
            R.id.tv_motion_filter -> {
                changeButtonColor(tv_motion_filter)
                filterRecords("Motion")
            }
            R.id.tv_stop_filter -> {
                changeButtonColor(tv_stop_filter)
                filterRecords("Stop")
            }
            R.id.tv_unassigned_filter -> {
                changeButtonColor(tv_unassigned_filter)
                filterRecords("Un Assigned")
            }
            R.id.tv_unlock_filter -> {
                changeButtonColor(tv_unlock_filter)
                filterRecords("Unlock")
            }
            R.id.iv_toolbar_search_icon ->{
                toggleSearchBarVisibility()
            }
            R.id.iv_searchbar_back -> {
                toggleSearchBarVisibility()
                sv_toolbar_search!!.text.clear()
            }

        }
    }

    fun toggleSearchBarVisibility() {
        if (searchbar?.visibility == View.GONE) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            searchbar?.visibility = View.VISIBLE
            toolbars?.visibility = View.GONE
            sv_toolbar_search?.requestFocus()
            sv_toolbar_search!!.text.clear()

        } else {
            Utils.hideSoftKeyboard(this)
            sv_toolbar_search?.clearFocus()
            searchbar?.visibility = View.GONE
            toolbars?.visibility = View.VISIBLE

        }
    }

    private fun itemClickListener(): GridViewAdapter.OnItemClickListener {
        return object : GridViewAdapter.OnItemClickListener {

            override fun onClickMapView(position: Int, deviceModel: GridListModel) {
                Toast.makeText(this@GridViewActivity, deviceModel.iotName, Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun filterRecords(status: String?) {
        tv_no_data.visibility = View.GONE
        grid_list_recycler_view!!.visibility = View.VISIBLE
        deviceList.removeAll(deviceList)
        //deviceList.clear()

        when (status) {
            Constant.RequestStatus.ALL.value ->{
                if (mainDeviceList.size > 0) {
                    deviceList.addAll(mainDeviceList)

                    gridViewAdapter =
                        GridViewAdapter(
                            this,
                            deviceList,
                            itemClickListener()
                        )

                    grid_list_recycler_view?.adapter = gridViewAdapter

                     gridViewAdapter!!.notifyDataSetChanged()
                     grid_list_recycler_view!!.visibility = View.VISIBLE

                   // grid_list_recycler_view?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                    //grid_list_recycler_view?.adapter = gridViewAdapter
                    //gridViewAdapter!!.notifyDataSetChanged()
                    //grid_list_recycler_view!!.visibility = View.VISIBLE

                } else {
                    grid_list_recycler_view!!.visibility = View.GONE
                    tv_no_data.text = "No Data Found"
                    tv_no_data.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                        "No Data Found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            Constant.RequestStatus.ASSIGNED.value ->{
                if (mainDeviceList.size > 0) {
                    mainDeviceList.let { it1 ->
                        it1.forEach {
                            listStatus = it.queryStatus
                            if (listStatus == status) {
                                val pendingList = ArrayList<GridListModel>()
                                pendingList.add(it)
                                deviceList.addAll(pendingList)

                            }
//                        tvReadyForInstalltion?.append(" (${listForAdapter.size}) ")
                        }
                    }
                    if (deviceList.isNotEmpty()) {
                        gridViewAdapter =
                            GridViewAdapter(
                                this,
                                deviceList,
                                itemClickListener()
                            )

                        grid_list_recycler_view?.adapter = gridViewAdapter
                        gridViewAdapter!!.notifyDataSetChanged()
                        grid_list_recycler_view!!.visibility = View.VISIBLE
                    } else {
                        grid_list_recycler_view!!.visibility = View.GONE
                        tv_no_data.text = "No Data Found"
                        tv_no_data.visibility = View.VISIBLE
                    }
                } else {
                    grid_list_recycler_view!!.visibility = View.GONE
                    tv_no_data.text = "No Data Found"
                    tv_no_data.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                       "No Data Found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            Constant.RequestStatus.AVAILABLE.value ->{
                if (mainDeviceList.size > 0) {
                    mainDeviceList.let { it1 ->
                        it1.forEach {
                            listStatus = it.queryStatus
                            if (listStatus == status) {
                                val pendingList = ArrayList<GridListModel>()
                                pendingList.add(it)
                                deviceList.addAll(pendingList)

                            }
//                        tvReadyForInstalltion?.append(" (${listForAdapter.size}) ")
                        }
                    }
                    if (deviceList.isNotEmpty()) {
                        gridViewAdapter =
                            GridViewAdapter(
                                this,
                                deviceList,
                                itemClickListener()
                            )

                        grid_list_recycler_view?.adapter = gridViewAdapter
                        gridViewAdapter!!.notifyDataSetChanged()
                        grid_list_recycler_view!!.visibility = View.VISIBLE
                    } else {
                        grid_list_recycler_view!!.visibility = View.GONE
                        tv_no_data.text = "No Data Found"
                        tv_no_data.visibility = View.VISIBLE
                    }
                } else {
                    grid_list_recycler_view!!.visibility = View.GONE
                    tv_no_data.text = "No Data Found"
                    tv_no_data.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                        "No Data Found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            Constant.RequestStatus.DISCONNECTED.value ->{
                if (mainDeviceList.size > 0) {
                    mainDeviceList.let { it1 ->
                        it1.forEach {
                            listStatus = it.queryStatus
                            if (listStatus == status) {
                                val pendingList = ArrayList<GridListModel>()
                                pendingList.add(it)
                                deviceList.addAll(pendingList)

                            }
//                        tvReadyForInstalltion?.append(" (${listForAdapter.size}) ")
                        }
                    }
                    if (deviceList.isNotEmpty()) {
                        gridViewAdapter =
                            GridViewAdapter(
                                this,
                                deviceList,
                                itemClickListener()
                            )

                        grid_list_recycler_view?.adapter = gridViewAdapter
                        gridViewAdapter!!.notifyDataSetChanged()
                        grid_list_recycler_view!!.visibility = View.VISIBLE
                    } else {
                        grid_list_recycler_view!!.visibility = View.GONE
                        tv_no_data.text = "No Data Found"
                        tv_no_data.visibility = View.VISIBLE
                    }
                } else {
                    grid_list_recycler_view!!.visibility = View.GONE
                    tv_no_data.text = "No Data Found"
                    tv_no_data.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                        "No Data Found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            Constant.RequestStatus.IDLE.value ->{
                if (mainDeviceList.size > 0) {
                    mainDeviceList.let { it1 ->
                        it1.forEach {
                            listStatus = it.queryStatus
                            if (listStatus == status) {
                                val pendingList = ArrayList<GridListModel>()
                                pendingList.add(it)
                                deviceList.addAll(pendingList)

                            }
//                        tvReadyForInstalltion?.append(" (${listForAdapter.size}) ")
                        }
                    }
                    if (deviceList.isNotEmpty()) {
                        gridViewAdapter =
                            GridViewAdapter(
                                this,
                                deviceList,
                                itemClickListener()
                            )

                        grid_list_recycler_view?.adapter = gridViewAdapter
                        gridViewAdapter!!.notifyDataSetChanged()
                        grid_list_recycler_view!!.visibility = View.VISIBLE
                    } else {
                        grid_list_recycler_view!!.visibility = View.GONE
                        tv_no_data.text = "No Data Found"
                        tv_no_data.visibility = View.VISIBLE
                    }
                } else {
                    grid_list_recycler_view!!.visibility = View.GONE
                    tv_no_data.text = "No Data Found"
                    tv_no_data.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                        "No Data Found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            Constant.RequestStatus.LOCK.value ->{
                if (mainDeviceList.size > 0) {
                    mainDeviceList.let { it1 ->
                        it1.forEach {
                            listStatus = it.queryStatus
                            if (listStatus == status) {
                                val pendingList = ArrayList<GridListModel>()
                                pendingList.add(it)
                                deviceList.addAll(pendingList)

                            }
//                        tvReadyForInstalltion?.append(" (${listForAdapter.size}) ")
                        }
                    }
                    if (deviceList.isNotEmpty()) {
                        gridViewAdapter =
                            GridViewAdapter(
                                this,
                                deviceList,
                                itemClickListener()
                            )

                        grid_list_recycler_view?.adapter = gridViewAdapter
                        gridViewAdapter!!.notifyDataSetChanged()
                        grid_list_recycler_view!!.visibility = View.VISIBLE
                    } else {
                        grid_list_recycler_view!!.visibility = View.GONE
                        tv_no_data.text = "No Data Found"
                        tv_no_data.visibility = View.VISIBLE
                    }
                } else {
                    grid_list_recycler_view!!.visibility = View.GONE
                    tv_no_data.text = "No Data Found"
                    tv_no_data.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                        "No Data Found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            Constant.RequestStatus.MOTION.value ->{
                if (mainDeviceList.size > 0) {
                    mainDeviceList.let { it1 ->
                        it1.forEach {
                            listStatus = it.queryStatus
                            if (listStatus == status) {
                                val pendingList = ArrayList<GridListModel>()
                                pendingList.add(it)
                                deviceList.addAll(pendingList)

                            }
//                        tvReadyForInstalltion?.append(" (${listForAdapter.size}) ")
                        }
                    }
                    if (deviceList.isNotEmpty()) {
                        gridViewAdapter =
                            GridViewAdapter(
                                this,
                                deviceList,
                                itemClickListener()
                            )

                        grid_list_recycler_view?.adapter = gridViewAdapter
                        gridViewAdapter!!.notifyDataSetChanged()
                        grid_list_recycler_view!!.visibility = View.VISIBLE
                    } else {
                        grid_list_recycler_view!!.visibility = View.GONE
                        tv_no_data.text = "No Data Found"
                        tv_no_data.visibility = View.VISIBLE
                    }
                } else {
                    grid_list_recycler_view!!.visibility = View.GONE
                    tv_no_data.text = "No Data Found"
                    tv_no_data.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                        "No Data Found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            Constant.RequestStatus.STOP.value ->{
                if (mainDeviceList.size > 0) {
                    mainDeviceList.let { it1 ->
                        it1.forEach {
                            listStatus = it.queryStatus
                            if (listStatus == status) {
                                val pendingList = ArrayList<GridListModel>()
                                pendingList.add(it)
                                deviceList.addAll(pendingList)

                            }
//                        tvReadyForInstalltion?.append(" (${listForAdapter.size}) ")
                        }
                    }
                    if (deviceList.isNotEmpty()) {
                        gridViewAdapter =
                            GridViewAdapter(
                                this,
                                deviceList,
                                itemClickListener()
                            )

                        grid_list_recycler_view?.adapter = gridViewAdapter
                        gridViewAdapter!!.notifyDataSetChanged()
                        grid_list_recycler_view!!.visibility = View.VISIBLE
                    } else {
                        grid_list_recycler_view!!.visibility = View.GONE
                        tv_no_data.text = "No Data Found"
                        tv_no_data.visibility = View.VISIBLE
                    }
                } else {
                    grid_list_recycler_view!!.visibility = View.GONE
                    tv_no_data.text = "No Data Found"
                    tv_no_data.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                        "No Data Found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            Constant.RequestStatus.UN_ASSIGNED.value ->{
                if (mainDeviceList.size > 0) {
                    mainDeviceList.let { it1 ->
                        it1.forEach {
                            listStatus = it.queryStatus
                            if (listStatus == status) {
                                val pendingList = ArrayList<GridListModel>()
                                pendingList.add(it)
                                deviceList.addAll(pendingList)

                            }
//                        tvReadyForInstalltion?.append(" (${listForAdapter.size}) ")
                        }
                    }
                    if (deviceList.isNotEmpty()) {
                        gridViewAdapter =
                            GridViewAdapter(
                                this,
                                deviceList,
                                itemClickListener()
                            )

                        grid_list_recycler_view?.adapter = gridViewAdapter
                        gridViewAdapter!!.notifyDataSetChanged()
                        grid_list_recycler_view!!.visibility = View.VISIBLE
                    } else {
                        grid_list_recycler_view!!.visibility = View.GONE
                        tv_no_data.text = "No Data Found"
                        tv_no_data.visibility = View.VISIBLE
                    }
                } else {
                    grid_list_recycler_view!!.visibility = View.GONE
                    tv_no_data.text = "No Data Found"
                    tv_no_data.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                        "No Data Found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            Constant.RequestStatus.UNLOCK.value ->{
                if (mainDeviceList.size > 0) {
                    mainDeviceList.let { it1 ->
                        it1.forEach {
                            listStatus = it.queryStatus
                            if (listStatus == status) {
                                val pendingList = ArrayList<GridListModel>()
                                pendingList.add(it)
                                deviceList.addAll(pendingList)

                            }
//                        tvReadyForInstalltion?.append(" (${listForAdapter.size}) ")
                        }
                    }
                    if (deviceList.isNotEmpty()) {
                        gridViewAdapter =
                            GridViewAdapter(
                                this,
                                deviceList,
                                itemClickListener()
                            )

                        grid_list_recycler_view?.adapter = gridViewAdapter
                        gridViewAdapter!!.notifyDataSetChanged()
                        grid_list_recycler_view!!.visibility = View.VISIBLE
                    } else {
                        grid_list_recycler_view!!.visibility = View.GONE
                        tv_no_data.text = "No Data Found"
                        tv_no_data.visibility = View.VISIBLE
                    }
                } else {
                    grid_list_recycler_view!!.visibility = View.GONE
                    tv_no_data.text = "No Data Found"
                    tv_no_data.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                        "No Data Found",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        }



    }
}