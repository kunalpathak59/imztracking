package com.imzcorporate.activities

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentManager
import com.imzcorporate.R
import com.imzcorporate.fragment.BatteryDetailFragment
import com.imzcorporate.fragment.FilterListFragment
import com.imzcorporate.fragment.OtherFragment
import com.imzcorporate.fragment.QuickViewFragment
import com.imzcorporate.model.FilterListModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_navigation_fragment.*
import kotlinx.android.synthetic.main.toolbar_dashboard.*

class DashBoardActivity : AppCompatActivity(), View.OnClickListener {

    private var profileClicked = false
    private var reportClicked = false
    private val quickviewFragmentTag = "quik_view_fragment"
    private val batteryFragmentTag = "battery_fragment"
    private val otherFragmentTag = "other_fragment"
    private var quickViewFragment = QuickViewFragment()
    private var batteryDetailFragment = BatteryDetailFragment()
    private var otherFragment = OtherFragment()
    private var accountArrayFilterListModel = ArrayList<FilterListModel>()
    private var sub_accountArrayFilterListModel = ArrayList<FilterListModel>()
    private var selectedAccountId = 0
    private var selectedSubAccountId = 0
    private var btnArray: Array<Button>? = null
    private var fragmentCallerType = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_)

        initListener()
        setData()
        setFragmentForInstallation(1)
        changeButtonColor(btnQuickView)

    }

     fun initListener() {
         tv_toolbar_title?.text = "Dashboard"

         tv_change_password.visibility = View.GONE
         llReoprt.visibility = View.GONE

         iv_menu.setOnClickListener(this)
         atvMainAccount.setOnClickListener(this)
         atvsubAccount.setOnClickListener(this)
         ivMainAccountClear.setOnClickListener(this)
         ivSubAccountClear.setOnClickListener(this)
         btnQuickView.setOnClickListener(this)
         btnBatteryView.setOnClickListener(this)
         btnOther.setOnClickListener(this)
         iv_menu.setImageResource(R.drawable.menu_btn)

    }

    fun setData(){
        setUpAtvAdapter()
        btnArray = arrayOf(btnQuickView, btnBatteryView, btnOther)
    }


    fun navigationClick(view: View?) {
        when (view) {
            ivArrow -> {
                if (!profileClicked) {
                    profileClicked = true
                    ivArrow.setImageResource(R.drawable.icon_arrow_down)
                    tv_change_password.visibility = View.VISIBLE
                } else {
                    profileClicked = false
                    ivArrow.setImageResource(R.drawable.icon_arrow_forward)
                    tv_change_password.visibility = View.GONE
                }
            }

            tv_change_password -> {
            }

            tv_home -> {
                drawer_layout.closeDrawer(Gravity.LEFT)
            }

            rlReportHeader -> {
                if (!reportClicked) {
                    reportClicked = true
                    llReoprt.visibility = View.VISIBLE
                    ivReportCollapseExpand.setImageResource(R.drawable.icon_arrow_down)
                } else {
                    reportClicked = false
                    llReoprt.visibility = View.GONE
                    ivReportCollapseExpand.setImageResource(R.drawable.icon_arrow_forward)
                }
            }

            tv_distance_report -> {

                reportClicked = false
                llReoprt.visibility = View.GONE
                ivReportCollapseExpand.setImageResource(R.drawable.icon_arrow_forward)
                drawer_layout.closeDrawer(Gravity.LEFT)

            }

            tv_alerts_report -> {
               // startActivity(Intent(this, AlertsNotificationActivity::class.java))
                drawer_layout.closeDrawer(Gravity.LEFT)
            }

            tv_logout -> {
                drawer_layout.closeDrawer(Gravity.LEFT)
               // PreferenceUtils.clearPref(this)
                startActivity(Intent(this, LoginActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                })

                finish()
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_menu -> {
                drawer_layout.openDrawer(GravityCompat.START)
            }
            R.id.atvMainAccount -> {
                callFilterListFragment(accountArrayFilterListModel, atvMainAccount, 1,"Select Account")
            }
            R.id.atvsubAccount -> {
                callFilterListFragment(sub_accountArrayFilterListModel, atvsubAccount, 2,"Select Sub Account")
            }
            R.id.ivMainAccountClear -> {
                filterAtvClear(atvMainAccount, ivMainAccountClear)
            }
            R.id.ivSubAccountClear -> {
                filterAtvClear(atvsubAccount, ivSubAccountClear)
            }
            R.id.btnQuickView -> {
                changeButtonColor(btnQuickView)
                setFragmentForInstallation(1)
            }
            R.id.btnBatteryView -> {
                changeButtonColor(btnBatteryView)
                setFragmentForInstallation(2)
            }
            R.id.btnOther -> {
                changeButtonColor(btnOther)
                setFragmentForInstallation(3)
            }

        }
    }

    private fun filterAtvClear(atv: AutoCompleteTextView, ivCross: ImageView) {
        atv.setText("")
        ivCross.visibility = View.GONE
    }

    private fun setUpAtvAdapter() {
        try {
            val arraySubAccount = ArrayList<FilterListModel>()
            val arrayAccount = ArrayList<FilterListModel>()

            for (j in 0 until 10) {
                val subAccount = FilterListModel()
                subAccount.name = "Sub Account $j"
                subAccount.id = j+1
                arraySubAccount.add(subAccount)
            }
            for (k in 0 until 10) {
                val accountName = FilterListModel()
                accountName.name = "Account $k"
                accountName.id = k+1
                arrayAccount.add(accountName)
            }

            sub_accountArrayFilterListModel.addAll(arraySubAccount)
            accountArrayFilterListModel.addAll(arrayAccount)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun callFilterListFragment(
        arrayFilterListModel: ArrayList<FilterListModel>,
        atv: AutoCompleteTextView,
        caller: Int,
        title: String) {
        val filterListFragment = FilterListFragment(this, arrayFilterListModel, title)
        filterListFragment.setOnDeviceSelectionListener(object :
            FilterListFragment.OnDeviceSelectionListener {
            override fun onDeviceSelected(filterListModel: FilterListModel) {
                atv.setText(filterListModel.name)
                when (caller) {
                    1 -> {
                        ivMainAccountClear.visibility = View.VISIBLE
                        selectedAccountId = filterListModel.id
                        Log.i("AccountId:", selectedAccountId.toString())
                        //vehicleListOnClickListener()
                    }
                    2 -> {
                        ivSubAccountClear.visibility = View.VISIBLE
                        selectedSubAccountId = filterListModel.id
                        Log.i("Sub AccountId:", selectedSubAccountId.toString())
                        //vehicleListOnClickListener()
                    }
                }
            }
        })

        filterListFragment.show(supportFragmentManager, FilterListFragment.TAG)
    }

    private fun changeButtonColor(clickedButton: Button) {
        btnArray!!.forEach {
            if (it == clickedButton) {
                it.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.oliveGreen)
                it.setTextColor(Color.parseColor("#ffffff"))
            } else {
                it.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.white)
                it.setTextColor(Color.parseColor("#219797"))

            }
        }
    }

    private fun setFragmentForInstallation(screenPosition: Int) {

        when (screenPosition) {
            1 -> {
                fragmentCallerType = 1
                supportFragmentManager.beginTransaction()
                    .replace(R.id.frameLayoutDashboard, quickViewFragment)
                    .addToBackStack(quickviewFragmentTag).commit()
            }
            2 -> {
                fragmentCallerType = 2
                supportFragmentManager.beginTransaction()
                    .replace(R.id.frameLayoutDashboard, batteryDetailFragment)
                    .addToBackStack(batteryFragmentTag).commit()
            }
            3 -> {
                fragmentCallerType = 3
                supportFragmentManager.beginTransaction()
                    .replace(R.id.frameLayoutDashboard, otherFragment)
                    .addToBackStack(otherFragmentTag).commit()
            }
        }

    }

    override fun onBackPressed() {
        val fm: FragmentManager = supportFragmentManager
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStack()
        }
        finish()
        super.onBackPressed()
    }
}