package com.imzcorporate.activities

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.maps.android.clustering.ClusterManager
import com.imzcorporate.R
import com.imzcorporate.model.Vehicle_ItemDetail
import com.imzcorporate.util.CustomInfoWindowForGoogleMap
import kotlinx.android.synthetic.main.layout_bottom_sheet.*
import kotlinx.android.synthetic.main.toolbar_gradient.*

class MapsViewActivity : AppCompatActivity(), OnMapReadyCallback,
    View.OnClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var mClusterManager: ClusterManager<Vehicle_ItemDetail>
   // private var isShowing:Boolean = false
    //private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_maps_view)

        initView()
        initListner()
    }

    private fun initView() {
        tv_toolbar_title_.text = "Map View"
        iv_toolbar_search_card.visibility = View.GONE

        // bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.maps) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun initListner() {

        iv_menu_back.setOnClickListener(this)

       /* bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // handle onSlide
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> Toast.makeText(
                        this@MapsViewActivity,
                        "STATE_COLLAPSED",
                        Toast.LENGTH_SHORT
                    ).show()
                    BottomSheetBehavior.STATE_EXPANDED -> Toast.makeText(
                        this@MapsViewActivity,
                        "STATE_EXPANDED",
                        Toast.LENGTH_SHORT
                    ).show()
                    BottomSheetBehavior.STATE_DRAGGING -> Toast.makeText(
                        this@MapsViewActivity,
                        "STATE_DRAGGING",
                        Toast.LENGTH_SHORT
                    ).show()
                    BottomSheetBehavior.STATE_SETTLING -> Toast.makeText(
                        this@MapsViewActivity,
                        "STATE_SETTLING",
                        Toast.LENGTH_SHORT
                    ).show()
                    BottomSheetBehavior.STATE_HIDDEN -> Toast.makeText(
                        this@MapsViewActivity,
                        "STATE_HIDDEN",
                        Toast.LENGTH_SHORT
                    ).show()
                    else -> Toast.makeText(this@MapsViewActivity, "OTHER_STATE", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })*/
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setUpClusterer()

        mClusterManager.setOnClusterClickListener {
            Toast.makeText(this, "Cluster click", Toast.LENGTH_SHORT).show()
            // if true, do not move camera
            false
        }
        mClusterManager.setOnClusterItemClickListener {

          /*  if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

            }*/
            bottomSheet.visibility = View.VISIBLE

           /* if (isShowing){
                bottomSheet.visibility = View.VISIBLE
                isShowing = false
            }else{
                bottomSheet.visibility = View.GONE
                isShowing = true
            }*/

            Toast.makeText(this, "Cluster item click", Toast.LENGTH_SHORT).show()
            // if true, click handling stops here and do not show info view, do not move camera
            // you can avoid this by calling:
            // renderer.getMarker(clusterItem).showInfoWindow();
            false
        }

        // mMap.setInfoWindowAdapter(CustomInfoWindowForGoogleMap(this))

       /* mMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            @SuppressLint("PotentialBehaviorOverride")
            override fun getInfoWindow(marker: Marker): View {
                var mView: View? = null
                try {
                    mView = getLayoutInflater().inflate(R.layout.custom_marker_layout, null);

                } catch (ev: Exception) {
                    print(ev.message)
                }
                return mView!!
            }

            override fun getInfoContents(marker: Marker): View? {
                return null
            }
        }) */
    }

    private fun setUpClusterer() {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng( 28.4962483, 77.0788852), 10f))
        mClusterManager = ClusterManager(this, mMap)
        mMap.setOnCameraIdleListener(mClusterManager)
        mMap.setOnMarkerClickListener(mClusterManager)
        addItems()
        mClusterManager.cluster()


        mClusterManager.markerCollection.setInfoWindowAdapter(CustomInfoWindowForGoogleMap(this))

        mMap.setInfoWindowAdapter(mClusterManager.markerManager)

        // mClusterManager.renderer = MarkerClusterRenderer(this, mMap, mClusterManager)
    }

    private fun addItems() {
        var lat = 28.4962483
        var lng = 77.0788852
        for (i in 0..9) {
            val offset = i / 60.0
            lat += offset
            lng += offset
            val title = "This is the title"
            val snippet = "and this is the snippet."
            val offsetItem = Vehicle_ItemDetail(lat, lng, title, snippet)
            mClusterManager.addItem(offsetItem)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_menu_back -> {
                finish()
            }
        }
    }
}